#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2023,2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only


import sympy

from ..loonies.symbolic import Z


def apply_tax_ar(A, R, income):
	"""
	Compute tax using all brackets at their marginal rate (as God intended)
	"""

	out = 0

	for i in range(len(A)):
		a = Z(A[i])
		r = Z(R[i])

		try:
			b = Z(A[i+1])
			c = sympy.Piecewise(
			 (b, b <= income),
			 (income, True),
			)
		except IndexError:
			b = income
			c = b

		if i == 0:
			chunk = (c-a) * r
		else:
			chunk = sympy.Piecewise(
			 (0, income < a),
			 ((c-a) * r, True),
			)

		out += chunk

	return out



def apply_tax_ark(A, R, K, income):
	"""
	Compute tax using the 3-coefficient (income * (R-K) if income > A)
	method used by some forms.
	"""
	args = []
	for a, r, k in [x for x in zip(A,R,K)][::-1]:
		args += [((income * Z(r) - Z(k)), income >= Z(a))]

	return sympy.Piecewise(*args)
