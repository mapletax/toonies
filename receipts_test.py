#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Rostyslav Lobov <rostyslav-mapletax@exmakhina.com> & contributors
# SPDX-Licence-Identifier: AGPL-3.0-only

import logging
import numbers
import typing as t

from .receipts import *


logger = logging.getLogger(__name__)


def test_receipts():
	s1 = Receipt("medical", amount=123.45)
	s2 = Receipt("medical", amount=123.45)
	receipts = Receipts([s1, s2])

	assert receipts["medical"]["amount"] == Z("246.90")
