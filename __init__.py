#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-mapletax@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# MapleTax Toonies

"""
This package contains most of the flesh of mapletax.

The `formyula.Formyula` class provides syntactic sugar for expressing the forms in code,
several `formyula.Formyula` instances (corresponding to several forms, typically one
form and several schedules or annexes) can be put together in a `ca.forms.Forms`
or `qc.forms.Forms` container, which allows a simpler entry points to them.

The `Forms` container is created by the corresponding `forms` module, factory-style,
to instantiate the proper forms and schedules/annexes depending on the year and province.

The `formyula.Formyula` can be populated by code corresponding to forms, see for example
`ca.t1_5005_2022` which populates the Québec T1 for 2022.
Such "populating" code is expressed in the way that is the most transparent depending
on the form.
"""
