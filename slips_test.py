#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Rostyslav Lobov <rostyslav-mapletax@exmakhina.com> & contributors
# SPDX-Licence-Identifier: AGPL-3.0-only

import logging
import numbers
import typing as t

from .slips import *


logger = logging.getLogger(__name__)


def test_slips():
	s1 = Slip("RL-1", A=123.45)
	s2 = Slip("RL-1", A=123.45)
	slips = Slips([s1, s2])

	assert slips["RL-1"]["A"] == Z("246.90")

def test_slips_invalid():
	try:
		s1 = Slip("RL-123")
		assert 0
	except ValueError:
		pass

def test_slips_ca_qc():
	assert federal_slip_p("T4")
	assert quebec_slip_p("RL-1")
	assert not federal_slip_p("RL-1")
	assert not quebec_slip_p("T4")
	assert not federal_slip_p("abc")
	assert not quebec_slip_p("def")
