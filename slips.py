#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Rostyslav Lobov <rostyslav-mapletax@exmakhina.com> & contributors
# SPDX-Licence-Identifier: AGPL-3.0-only

import typing as t
import logging
import numbers
import functools
import re

from ..loonies.symbolic import Z


logger = logging.getLogger(__name__)


def get_federal_slips():
	"""
	https://www.canada.ca/en/revenue-agency/services/tax/individuals/topics/about-your-tax-return/tax-return/completing-a-tax-return/tax-slips.html
	"""

	r"emacs replace regexp  ^\(.*?\) \(.*\) → \1\t\2"

	lines = """
T4\tStatement of Remuneration Paid
T4A\tStatement of Pension, Retirement, Annuity, and Other Income
T4A(OAS)\tStatement of Old Age Security
T4A(P)\tStatement of Canada Pension Plan Benefits
T4E\tStatement of Employment Insurance and Other Benefits
T4FHSA\tFirst Home Savings Account Statement
T4RIF\tStatement of income from a Registered Retirement Income Fund
T4RSP\tStatement of RRSP Income
T5\tStatement of Investment Income - slip information for individuals
T5007\tStatement of Benefits
T5008\tStatement of Securities Transactions - slip information for individuals
T5013\tStatement of Partnership income
T5018\tStatement of Contract Payments
T3\tStatement of Trust Income Allocations and Designations – slip information for individuals
T2202\tTuition Enrolment Certificate
T1204\tGovernment Services Contract Payments
RC62\tUniversal Child Care Benefit statement
RC210\tWorking Income Tax Benefit Advance Payments Statement
RRSP\tcontribution receipt – slip information for individuals
PRPP\tcontribution receipt – slip information for individuals
""".strip().splitlines()
	return dict(line.split("\t") for line in lines)


@functools.lru_cache
def federal_slip_p(sliptype: str) -> bool:
	"""
	:return: whether a slip type is federal
	"""
	federal_slips = get_federal_slips()
	return sliptype in federal_slips


def quebec_slip_p(sliptype) -> bool:
	"""
	:return: whether a slip type is Québécois
	https://www.revenuquebec.ca/en/businesses/rl-slips-and-summaries/rl-slips/
	"""
	if (m := re.match(r"^RL-(?P<n>\d+)$", sliptype)) is None:
		return False
	n = int(m.group("n"))
	return 1 <= n <= 32


class Slip:
	"""
	Class to store the slip data. It is possible to assign the data upon creation,
	by passing the dict of values or by passing values as a key-word arguments
	"""
	def __init__(self,
	 sliptype: str,
	 values: t.Mapping[str, numbers.Number]=None,
	 **kw
	):
		"""
		:param sliptype: Type of the slip, eg. "RLT-4"
		:param values: Dict with slip data. Exmpl. {"A": 15670}
		"""
		if not (federal_slip_p(sliptype) or quebec_slip_p(sliptype)):
			raise ValueError(f"Unrecognized slip type {sliptype}")

		self.sliptype = sliptype

		self.values = dict()
		if values is not None:
			for k,v in values.items():
				self[k] = v
		for k, v in kw.items():
			self[k] = v

	def update(self, d):
		self.values |= d

	def __setitem__(self, k, v):
		if isinstance(v, float):
			v = 100 * v
			if v != int(v):
				raise ValueError(v)
			v = Z(int(v)) / 100
		elif isinstance(v, t.Mapping):
			pass # pass structure as-is, it's just not summable
		elif isinstance(v, str):
			if quebec_slip_p(self.sliptype):
				# Special handling for Québec forms, which use French locale
				v = v.replace(" ", "").replace(",", ".")
			v = Z(v)
		elif isinstance(v, t.Sequence):
			# Special handling for radio buttons and are transformed into an integer
			out = None
			for idx_e, e in enumerate(v):
				if not isinstance(e, bool):
					raise ValueError(v)
				if e:
					out = idx_e
			v = out
		else:
			v = Z(v)

		self.values[k] = v

	def __getitem__(self, k):
		return self.values[k]

	def get(self, k, default=None):
		return self.values.get(k, default)


class SlipSummationProxy:
	"""
	Proxy for Slips '__getitem__' method.
	Intended to return the sum of the corresponding slip values
	"""
	def __init__(self, slips, selected):
		self.slips = slips
		self.selected = selected

	def __getitem__(self, k):
		res = 0
		for slip in self.selected:
			res += slip.get(k, 0)
		return res


class Slips:
	"""Container, intended to be initialized with all of a user's slips"""
	def __init__(self, slips: t.Sequence[Slip]):
		self.slips = slips

	def __getitem__(self, k):
		selected = []
		for slip in self.slips:
			if slip.sliptype == k:
				selected += [slip]
		return SlipSummationProxy(self, selected)

	def kinds(self):
		return { _.sliptype for _ in self.slips  }


def slips_from_info(data):
	"""
	Create a Slips instance from data structured like:

	.. code:: yaml

	   My employer:
	     T4:
	       14: ...

	     RLW-1:
	       A: ...

	   Daycare: ...

	"""
	slips = list()
	for source, info in data.items():
		for sliptype, slipdata in info.items():
			slip = Slip(sliptype, slipdata)
			slips.append(slip)
	return Slips(slips)
