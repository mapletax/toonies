#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# High-level tests

import logging

from .testing import requires_serviettes


@requires_serviettes()
def test_rita_2023():
	from mapletax.formfiller.serialization import load

	from mapletax.toonies.slips import slips_from_info
	from mapletax.toonies.ca import forms as ca_forms
	from mapletax.toonies.qc import forms as qc_forms

	logging.getLogger("mapletax.formfiller.draw").setLevel(logging.WARNING)

	info = load("samples/rita-2023.yml")
	year = info["year"]
	slips = slips_from_info(info["slips"])

	if 1:
		forms_qc = qc_forms.get_forms(year)
		forms_qc.apply_identification(info["identification"])
		forms_qc.apply_slips_information(slips)
		forms_qc.apply_receipts_information(info["receipts"])
		forms_qc.apply_overrides(info.get("overrides", {}))

	if 1:
		forms_ca = ca_forms.get_forms(year, "QC")
		forms_ca.apply_identification(info["identification"])
		forms_ca.apply_slips_information(slips)
		forms_ca.apply_receipts_information(info["receipts"])
		forms_ca.apply_overrides(info.get("overrides", {}))

	for k, v in forms_ca.forms.items():
		forms_qc.forms[k] = v

	for k, v in forms_qc.forms.items():
		forms_ca.forms[k] = v

	if 1:
		qc_forms.print_forms(forms_qc, "rita")

	if 1:
		ca_forms.print_forms(forms_ca, "rita")
