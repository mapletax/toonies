#!/usr/bin/env python#
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-mapletax@zougloub.eu> & contributors
# SPDX-Licence-Identifier: AGPL-3.0-only

import logging
import numbers
import typing as t


from .formyula import *
from .resolver import *


logger = logging.getLogger(__name__)


def test_formyula_basic():
	form = Formyula()
	form[1] = 2

	form[2] = form[1]

	form.add_alias(2, {45})

	resolver = Resolver([form])
	target = form[45]
	res = resolver(target)
	logger.info("form[45] = %s", res)
