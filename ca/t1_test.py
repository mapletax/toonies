#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Computations related to taxes

import os
import logging
import collections

import pytest

from ...loonies.currency import (
 R2D,
)

from ...loonies.symbolic import (
 Z,
)

from ..formyula import Formyula
from ..resolver import Resolver
from ..slips import Slips


logger = logging.getLogger(__name__)


def test_ca_2022():
	e = Formyula()
	slips = Slips([])

	from .t1_5005_2022 import (formulate, apply_slips_information, set_defaults)

	formulate(e)
	apply_slips_information(e, slips)
	set_defaults(e)

	e[30800] = 0
	e[22215] = 0

	s = e[169]
	resolver = Resolver([e])
	x = resolver(s)
	logger.info("Remboursement: %s", R2D(x))


serviettes = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "serviettes")

def requires_serviettes():
    import os
    return pytest.mark.skipif(not os.path.isdir(serviettes),
     reason=f"Missing serviettes at {serviettes}",
    )

@requires_serviettes()
def test_schedule_slips_dependencies():
	import os
	from .t1_5005_2022 import schedules
	from ...formfiller.serialization import load
	from ..slips import federal_slip_p

	slip2sch = collections.defaultdict(set)

	for nickname, (basename, desc) in schedules.items():
		path = os.path.join(serviettes, "2022", f"{basename}-fill-22e.json")
		meta = load(path)

		for page, page_data in meta.items():
			for name, info in page_data["fields"].items():
				desc = info.get("description")
				if not desc:
					continue
				for word in desc.split():
					if federal_slip_p(word):
						slip2sch[word].add(basename)

	logger.info("Slip -> sched deps: %s", dict(slip2sch))
