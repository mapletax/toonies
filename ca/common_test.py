#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2023,2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import logging

import sympy

from ..progressive_tax import (
 apply_tax_ark,
 apply_tax_ar,
)

from .common import *


logger = logging.getLogger(__name__)


def test_ar_vs_ark_symbolic():
	"""
	Show slight differences when computing tax using different formulas
	"""
	income_s = sympy.Symbol("income", positive=True)
	A, R, K = get_brackets(2023)
	v_ar = apply_tax_ar(A,R, income_s).simplify()
	v_ark = apply_tax_ark(A,R,K, income_s).simplify()
	v_diff = (v_ark - v_ar).simplify()
	logger.debug("  diff: %s", v_diff)
	for arg, cond in v_diff.args:
		assert abs(arg) < 1/2


def test_bpaf_2022(year=2022):
	assert bpaf(155625, year) == 14398
	assert bpaf(221708, year) == 12719
	assert bpaf(155625-1, year) == 14398
	assert bpaf(221708+1, year) == 12719
	assert bpaf(Z(155625+221708)/2, year) == Z(14398+12719)/2
	assert bpaf(Z(155625*2+221708*3)/5, year) == Z(14398*2+12719*3)/5


def test_get_brackets():
	for year in range(2021, 2024+1):
		br = get_brackets(year)
		logger.info("- %d: %s", year, br)
