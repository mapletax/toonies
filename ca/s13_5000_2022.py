#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Rostyslav Lobov <rostyslav@exmakhina.com> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Schedule 13 2022 (Employment Insurance Premiums on Schedule Self-Employment and Other Eligible Earnings)

import logging
import sympy

from ...loonies.symbolic import Z

logger = logging.getLogger(__name__)


def apply_slips_information(e: "Formyula", slips):
	e[54780] = slips["T4"][24] # insurable earnings TODO complicated formula


def _add_aliases(e: "Fromyula"):

	aliases = {
	 2: 54493,
	 3: 54494,
	 6: 54780,
	}

	for k, v in aliases.items():
		e.add_alias(v, {k})


def formulate(forms, e: "Formyula", year=2023):
	"""
	Setup Schedule 13 formulas.

	Reference: 5005-s13-22e.pdf
	"""

	_add_aliases(e)

	def s(x):
		return sympy.Symbol(x)

	# page 1
	e[1] = forms["T1"][11] + forms["T1"][25]

	e[4] = e[1] + e[2] + e[3]

	e[5] = Z(60300)

	e[7] = sympy.Max(0, e[5] - e[6])

	e[8] = sympy.Min(e[4], e[7])

	e[9] = sympy.Piecewise(
	 ((sympy.Min(Z("952.74"), e[8] * Z("0.0158"))), s("residence_province_is_qc") <= 0),
	 (0, True),
	)

	e[10] = sympy.Piecewise(
	 ((sympy.Min(Z("723.60"), e[8] * Z("0.0120"))), s("residence_province_is_qc") > 0),
	 (0, True),
	)

def apply_defaults(e: "Formyula"):
	e[2] = 0
	e[3] = 0
