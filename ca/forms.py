#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Federal forms

import os
import logging
import datetime
import re
import decimal
import collections

import sympy

from ...loonies.symbolic import Z

from .. import formyula


logger = logging.getLogger(__name__)


ca_global = object()

class Forms:
	"""
	Holder for federal forms
	"""
	def __init__(self, province, year):
		self.year = year
		self.province = province
		self.forms = {
		 ca_global: formyula.Formyula(""),
		}
		self._apply_slips_information = dict()
		self._schedules = dict()
		self._files = dict()

	def __setitem__(self, k, v):
		self.forms[k] = v

	def __getitem__(self, k):
		return self.forms[k]

	def apply_slips_information(self, allslips):
		for k, v in self.forms.items():
			if (func := self._apply_slips_information.get(k)) is None:
				continue
			func(v, allslips)

	def apply_identification(self, identification):
		self.identification = identification
		g = self.forms[ca_global]
		g["yob"] = identification["dob"].year
		g["self_employed"] = 1 if identification["self-employed"] else 0
		g["spouse"] = 1 if identification["spouse"] else 0
		province = identification["mailing"]["province"]
		province = identification.get("residence", dict()).get("province", province)
		g["residence_province_is_qc"] = 1 if province.upper() == "QC" else 0

		dependents = identification.get("dependents", dict())

		young_threshold = datetime.date(year=self.year-8, month=1, day=1)
		minor_threshold = datetime.date(year=self.year-18, month=1, day=1)
		future_threshold = datetime.date(year=self.year+1, month=1, day=1)

		impaireds = 0
		youngs = 0
		disableds = 0
		for idx_dependent, (nickname, dependent_info) in enumerate(dependents.items()):
			dob = dependent_info["dob"]#, "%Y-%m-%d").date()

			if dob >= future_threshold:
				continue

			if not dependent_info.get("child", True):
				continue

			is_young = dob >= young_threshold
			is_minor = dob >= minor_threshold
			is_disabled = dependent_info.get("infirm", False)
			is_impaired = dependent_info.get("impaired", False)

			if is_young and not is_disabled:
				youngs += 1

			elif is_disabled:
				disableds += 1

			elif is_minor or is_impaired:
				impaireds += 1


		g["nb_ca_disabled"] = disableds
		g["nb_ca_young_not_disabled"] = youngs
		g["nb_ca_minor_or_impaired"] = impaireds

	def apply_receipts_information(self, receipts):
		medical = 0
		childcare = 0
		for provider, info in receipts.items():
			for tp, entries in info.items():
				if tp == "healthcare-receipt":
					medical += entries["amount"]
				elif tp in ("daycare-receipt", "childcare-receipt"):
					childcare += entries["amount"]
				else:
					logger.warning("Skipping receipt %s %s", provider, tp)

		if medical > 0:
			logger.info("Adding %f of medical", medical)
			self.forms["T1"][33099] = Z(medical)
		if childcare > 0:
			logger.info("Adding %f of childcare", childcare)
			self.forms["T778"][67950] = Z(childcare)

	def apply_overrides(self, overrides):
		for formname, formoverrides in overrides.items():
			if formname == "-":
				formname = ca_global

			try:
				form = self.forms[formname]
			except KeyError:
				logger.info("Not applying overrides for form %s", formname)
				continue

			for k, v in formoverrides.items():
				logger.info("Applying %s %s=%s", formname, k, v)

				if isinstance(k, int):
					form[k] = Z(decimal.Decimal(v))
				else:
					if (m := re.match(r"page-(?P<page>\d+)", k)) is not None:
						page = int(m.group("page"))
						form_data = self.form_data.setdefault(formname, dict())
						page_data = form_data.setdefault(page, dict())
						for fieldid, value in v.items():
							if isinstance(value, bool):
								value = "Y"
							page_data[fieldid] = value
					else:
						form[k] = Z(decimal.Decimal(v))

def get_forms(year, province):
	"""
	"""

	forms = Forms(province, year=year)

	if province.upper() == "QC":

		if year == 2022:
			from . import t1_5005_2022 as t1
		elif year == 2023:
			from . import t1_5005_2023 as t1
		else:
			raise NotImplementedError(year)

		ft1 = formyula.Formyula("T1_")
		t1.formulate(ft1)
		t1.set_defaults(ft1)
		forms["T1"] = ft1
		forms._apply_slips_information["T1"] = t1.apply_slips_information
		forms._files["T1"] = "5005-r"

		forms._schedules = t1.schedules
		for nickname, (base, desc) in t1.schedules.items():
			f = formyula.Formyula(nickname + "_")
			sch = None
			if 0:
				pass
			elif nickname == "Sch2":
				if year == 2022:
					from . import s2_5005_2022 as sch
				if year == 2023:
					from . import s2_5005_2023 as sch
			elif nickname == "Sch8":
				if year == 2022:
					from . import s8_5005_2022 as sch
				if year == 2023:
					from . import s8_5005_2023 as sch
			elif nickname == "Sch13":
				if year == 2022:
					from . import s13_5000_2022 as sch
				if year == 2023:
					from . import s13_5000_2023 as sch

			if sch is not None:
				sch.formulate(forms, f)
				sch.apply_defaults(f)
				forms._apply_slips_information[nickname] = sch.apply_slips_information
			else:
				logger.warning("Schedule not handled: %s", nickname)

			forms[nickname] = f
			forms._files[nickname] = base

		for nickname in ["T778"]:
			f = formyula.Formyula(nickname + "_")
			sch = None
			if 0:
				pass
			elif nickname == "T778":
				if year == 2022:
					from . import t778_2022 as sch
				if year == 2023:
					from . import t778_2023 as sch

			if sch is not None:
				sch.formulate(forms, f)
				sch.apply_defaults(f)
				forms._apply_slips_information[nickname] = sch.apply_slips_information
			else:
				logger.warning("Form not handled: %s", nickname)

			forms[nickname] = f

		return forms

	raise NotImplementedError(province)


default = object()


def print_forms(forms, basename, schedules=None, filling=None):
	"""
	"""

	identification = forms.identification

	from ...formfiller.serialization import load
	from ...formfiller.draw import draw
	from ...formfiller.formfiller import collect_data
	from ...loonies.currency import (
	 R2D,
	)
	from ..resolver import Resolver

	serviettes = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "serviettes")

	resolver = Resolver(forms.forms.values())

	for nickname, base in forms._files.items():

		if schedules is not None:
			if nickname.startswith("Sch") and not int(nickname[3:]) in schedules:
				continue

		form_filling = dict()
		if filling is not None:
			form_filling = filling.get(nickname, dict())

		year = forms.year
		fn = os.path.join(serviettes, str(forms.year), base + "-fill-" + str(year % 100) + "e")
		meta_path = fn + ".json"
		fill_path = fn + ".pdf"
		meta = load(meta_path)

		e = forms[nickname]

		form_data = {}

		for page, pageinfo in meta.items():
			form_data[page] = {}
			logger.debug("Page: %s | fields: %s", page, len(pageinfo["fields"]))
			field_copy_number = collections.defaultdict(int)

			for k, v in pageinfo["fields"].items():

				value = default

				aliases = v.get("aliases", dict())

				if identification is not None and (alias := aliases.get("identity")) is not None:

					def dig(root, path):
						cur, *res = path
						if not res:
							return root[cur]
						return dig(root[cur], res)

					citizenship = identification["elections"]["citizenship"]
					if 0:
						pass
					elif identification["marital-status"] == alias:
						value = True
					elif alias == "mailing/address":
						mailing = identification.get("mailing", dict())
						n = mailing.get("street-number")
						street = mailing.get("street")
						if n and street:
							value = f"{n}, {street}"
					elif alias == "elections/citizenship" and citizenship:
						value = True
					elif alias == "elections/no-citizenship" and not citizenship:
						value = True
					elif (alias in ("elections/auth-for-election", "elections/no-auth-for-election")) \
								and citizenship:
						auth_for_election = identification["elections"]["auth-for-election"]
						if alias == "elections/auth-for-election" and auth_for_election:
							value = True
						elif alias == "elections/no-auth-for-election" and not auth_for_election:
							value = True

					elif alias == "foreign-property" and identification["foreign-property"]:
						value = True
					elif alias == "no-foreign-property" and not identification["foreign-property"]:
						value = True
					elif alias == "residence/province":
						value = identification.get("residence", {}).get("province")
						if value is None:
							value = mailing.get("province")
					else:
						try:
							value = dig(identification, alias.split("/"))
						except KeyError:
							pass

					date_fields = ("dob", "dod", "date-of-entry", "date-of-departure")

					if alias in ("spouse/sin", "sin", "mailing/postal-code") and value is not default:
						value = str(value).replace(" ", "")
					elif alias in date_fields and value is not default:
						value = str(value).replace("-", "")
					if alias in ("phone-number") and value is not default:
						value = str(value).translate(str.maketrans({"(": "", ")": "", "-": "", " ": ""}))

					if isinstance(value, bool):
						value = "Y" if value else ""

					if value is default:
						logger.warning("Couldn't resolve identity field %s", k)

				elif aliases:
					sym = None
					filling_value = default

					if (a := aliases.get("line")) is not None:
						sym = e[a]
						filling_value = form_filling.get(a, default)
					elif (a := aliases.get("stable")) is not None:
						sym = e[a]
						filling_value = form_filling.get(a, default)

					field_copy_number[sym] += 1

					if filling_value is not default:
						if isinstance(filling_value, list):
							value = filling_value[field_copy_number[sym]-1]
						else:
							value = filling_value
						logger.warning("Filling %s with %s", k, value)

					# Only print one column of income tax
					if any((
					  (year == 2022
					   and sym.name in ("T1_68", "T1_69", "T1_70", "T1_72", "T1_73", "T1_74", "T1_75")),
					  (year == 2023
					   and sym.name in ("T1_70", "T1_71", "T1_72", "T1_73", "T1_74", "T1_75", "T1_76")),
					 )):
						case_nr = int(resolver(forms["T1"]["income_case"]))
						if field_copy_number[sym] != case_nr:
							continue

					x = resolver(sym)
					if isinstance(x, int):
						value = x
					else:
						try:
							value = R2D(x)
						except (TypeError, NotImplementedError) as exc:
							logger.warning("%s (%s) is not calculated yet (%s): %s", sym, k, x, exc)

				if value is default:
					pass
				elif value is None:
					pass
				else:
					form_data[page][k] = value

		data = collect_data(meta, form_data)
		data = list(data)

		if nickname == "T1":
			if (signature := identification.get("signature")) is not None:
				data[-1]["fields"]["signature"] = {
				 #"debug-show-bbox": (1, (0, 1, 0, 1)),
				 "bounding-box": [110,535,190,25] if year == 2023 else [90,555,190,25],
				 "value": signature,
				}

			if (date := identification.get("date")) is not None:
				data[-1]["fields"]["date"] = {
				 #"debug-show-bbox": True,
				 "bounding-box": [80,590,100,15] if year == 2023 else [80,605,100,15],
				 "align": "center",
				 "font-color": [0, 0, 1, 1],
				 "value": date,
				}

		#logging.getLogger("mapletax.formfiller.draw").setLevel(logging.WARNING)

		dest = f"{basename}-{nickname}.pdf"
		draw(data, dest, fill_path)
