#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Computations related to taxes

import csv
import decimal
import functools
import io
import os
import codecs

import sympy

from ...loonies.symbolic import (
 Z,
)

from ...loonies.currency import (
 centround,
)

from ..progressive_tax import apply_tax_ark

def lerp(v, xa, xb, ya, yb):
	return sympy.Piecewise(
	 (ya, v <= xa),
	 (yb, v >= xb),
	 # no specification so we're using an unbiased rounding
	 # note that T4127, payroll deductions, uses round away from zero
	 (centround(ya + (v - xa) * Z(yb-ya) / (xb-xa)), True),
	)

def bpaf(v, year):
	"""
	"""
	if year == 2022:
		# Reference: 5000-d1-22e.pdf § Line 30000 – Basic personal amount
		return lerp(v, 155625, 221708, 14398, 12719)
	if year == 2023:
		# Reference: similar
		# Note that payroll deductions (T4127) has 13521...
		return lerp(v, 165430, 235675, 15000, 13520)
	if year == 2024:
		# TODO
		return lerp(v, 173205, 246752, 15705, 14156)
	raise NotImplementedError(year)


@functools.cache
def get_brackets(year):
	"""
	"""

	source= os.path.join(os.path.dirname(__file__),
	 f"rtsncmtrshldcnstnt-01-{year%2000}e.csv")

	def D_(x):
		return decimal.Decimal(x.replace(",", "").replace(" ", ""))

	with io.open(source, "rb") as fi:

		kw = dict()
		if year <= 2021:
			kw.update(
			 delimiter=";",
			)

		if year == 2024:
			class ByteStripper:
				def __init__(self, stream):
					self.stream = stream

				def read(self, n):
					return self.stream.read().replace(b"\xa0", b"")

			fi = ByteStripper(fi)

		codecinfo = codecs.lookup("utf-8")
		fi = codecinfo.streamreader(fi)

		reader = csv.reader(fi, **kw)
		next(reader)
		while True:
			r_a = next(reader)
			r_v = next(reader)
			r_k = next(reader)

			if r_a[0] == "Federal":
				r_a = [D_(x) for x in r_a[2:] if x]
				r_v = [D_(x) for x in r_v[2:] if x]
				r_k = [D_(x) for x in r_k[2:] if x]
				return r_a, r_v, r_k

def income_tax_ark(income, year):
	"""
	Compute tax using the last bracket
	"""
	A, R, K = get_brackets(year)
	return apply_tax_ark(A, R, K, income)

