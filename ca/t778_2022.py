#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import re

import sympy

from ...loonies.symbolic import Z


def _add_aliases(e: "Formyula"):

	aliases = {
	 2: 67960,
	 13: 67980,
	 18: 67990,
	}

	for k, v in aliases.items():
		e.add_alias(v, {k})


def formulate(forms, e: "Formyula", year=2022):
	"""
	Setup T778 - Child Care Expenses Deduction

	Reference: t778-fill-22e.pdf
	"""

	_add_aliases(e)

	# page 1

	def s(x):
		return sympy.Symbol(x)

	net_income = s("T1_net_income_for_t778")
	spouse_net_income = s("spouse.T1_net_income_for_t778")
	empty = s("empty")

	e[1] = s("nb_ca_young_not_disabled") * Z(8000)
	e[2] = s("nb_ca_disabled") * Z(11000)
	e[3] = s("nb_ca_minor_or_impaired") * Z(5000)
	e[4] = e[1] + e[2] + e[3]

	e[5] = e[67950]

	employment_income = s("T1_10100")
	net_self_employment_income = s("T1_25") # !unstable
	taxable_portion_of_scholarships = s("T1_13010")
	earnings_supplement = 0 # TODO
	disability_benefits = 0 # TODO
	amounts_under_apprenticeship_incentive = 0 # TODO
	covid19_payments = 0 # TODO

	earned_income = sympy.Add(
	 employment_income,
	 net_self_employment_income,
	 taxable_portion_of_scholarships,
	 earnings_supplement,
	 disability_benefits,
	 amounts_under_apprenticeship_incentive,
	 covid19_payments,
	)

	e[6] = earned_income * 2 / 3
	e[7] = sympy.Min(e[4], e[5], e[6])

	e[8] = sympy.Piecewise(
	 (empty, net_income >= spouse_net_income),
	 (s("spouse.T1_21400"), True),
	)

	e[9] = sympy.Piecewise(
	 (empty, net_income >= spouse_net_income),
	 (e[7] - e[8], True),
	)

	forms["T1"][21400] = sympy.Piecewise(
	 (e[14], net_income >= spouse_net_income),
	 (e[9], True),
	)

	e[10] = e[4] * Z("0.025")
	e[11]
	e[12]
	e[67980] = e[11] + e[12]
	e[14] = sympy.Min(e[7], e[13])

def apply_defaults(e: "Formyula"):
	e[67950] = 0
	e[67954] = 0
	e[11] = 0
	e[12] = 0
	pass

def apply_slips_information(e: "Formyula", slips):
	pass

# TODO apply receipts
