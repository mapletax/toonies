#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Rostyslav Lobov <rostyslav@exmakhina.com> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Schedule 8 2022 (Quebec Pension Plan Contributions)


import sympy

from ...loonies.symbolic import Z



def _add_aliases(e: "Formyula"):

	aliases = {
	 2: 50329,
	 6: 50330,
	}

	for k, v in aliases.items():
		e.add_alias(v, {k})

"""
Monthly proration table for 2022
 KEY: month number
 VALUE[0]: Maximum QPP pensionable earnings
 VALUE[1]: Maximum basic QPP exemption
"""
monthly_proration = {
	1:  (Z("5408.33"), Z("291.67")),
	2:  (Z("10816.68"), Z("583.33")),
	3:  (Z("16225.00"), Z("875.00")),
	4:  (Z("21633.33"), Z("1166.67")),
	5:  (Z("27041.67"), Z("1458.33")),
	6:  (Z("32450.00"), Z("1750.00")),
	7:  (Z("37858.33"), Z("2041.67")),
	8:  (Z("43266.67"), Z("2333.33")),
	9:  (Z("48675.00"), Z("2625.00")),
	10: (Z("54083.33"), Z("2916.67")),
	11: (Z("59491.67"), Z("3208.33")),
	12: (Z("64900.00"), Z("3500.00")),
}


# TODO only part2 is done.
def formulate(forms, e: "Formyula", year=2022):
	"""
	Setup Schedule 8 formulas.

	Reference: 5005-s8-22e.pdf
	"""

	_add_aliases(e)

	def s(x):
		return sympy.Symbol(x)

	employment_income = forms["T1"][10100]
	self_employment_income = s("AnxL_27")
	self_employed = s("self_employed") > 0
	not_self_employed = s("self_employed") <= 0
	employed = employment_income > 0
	not_employed = employment_income <= 0
	months = s("qpp_months")

	# page 2

	pieces = [(0, months <= 0)]
	for m, (a, b) in monthly_proration.items():
		pieces.append((a, months <= m))

	e[2.01] = sympy.Piecewise(*pieces)

	e[2.03] = sympy.Min(e[2.01], e[2.02])

	pieces = [(0, months <= 0)]
	for m, (a, b) in monthly_proration.items():
		pieces.append((b, months <= m))

	e[2.04] = sympy.Piecewise(*pieces)

	e[2.05] = sympy.Min(Z(61400), sympy.Max(0, e[2.03] - e[2.04]))

	e[2.07] = e[2.06] * Z("0.878049")

	e[2.08] = e[2.06] - e[2.07]

	e[2.09] = sympy.Min(Z("3315.60"), e[2.05] * Z("0.054"))

	e[2.10] = sympy.Min(Z(631), e[2.05] * Z("0.0075"))

	e[2.11] = e[2.09] + e[2.10]

	e[2.12] = e[2.06]

	e[2.13] = e[2.11]

	e[2.14] = sympy.Max(0, e[2.12] - e[2.13])

	# grep "of your return"


	# QPP contribution
	forms["T1"][30800] = sympy.Piecewise(
	 (
	  sympy.Min(e[2.07], e[2.09]),
	  sympy.And(not_self_employed, employed),
	 ),
	 (
	  0,
	  sympy.And(self_employed, not_employed),
	 ),
	 (
	  sympy.Min(e[4.26], e[4.27]),
	  sympy.And(self_employed, employed),
	 ),
	 (0, True),
	)

	forms["T1"][22215] = sympy.Piecewise(
	 (
	  sympy.Min(e[2.08], e[2.10]),
	  sympy.And(not_self_employed, employed),
	 ),
	 (
	  0,
	  sympy.And(self_employed, not_employed),
	 ),
	 (
	  sympy.Min(e[4.30], e[4.31]),
	  sympy.And(self_employed, employed),
	 ),
	 (0, True),
	)

	forms["T1"][22200] = sympy.Piecewise(
	 (
	  0,
	  sympy.And(not_self_employed, employed),
	 ),
	 (
	  e[3.09] + e[3.10],
	  sympy.And(self_employed, not_employed),
	 ),
	 (
	  s("todo"),
	  sympy.And(self_employed, employed),
	 ),
	 (0, True),
	)

def apply_defaults(e: "Formyula"):
	pass

def apply_slips_information(e: "Formyula", slips):
	e[2.02] = sympy.Piecewise(
	 (slips["T4"][14], slips["T4"][26] <= 0),
	 (slips["T4"][26], True),
	)

	e[2.06] = slips["T4"][17]
