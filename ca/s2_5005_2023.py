#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import re

import sympy

from ...loonies.symbolic import Z


def _add_aliases(e: "Formyula"):

	aliases = {
	 1: 35200,
	 2: 35300,
	 3: 35500,
	 4: 35700,
	 5: 36000,
	 12: 36100,
	}

	for k, v in aliases.items():
		e.add_alias(v, {k})


def formulate(forms, e: "Formyula", year=2022):
	"""
	Setup schedule 2 (Common law) formulas.

	Reference: 5005-s2-fill-22e.pdf
	"""

	_add_aliases(e)

	# page 1

	def s(x):
		return sympy.Symbol(x)


	e[35200] = sympy.Piecewise(
	 (s("spouse.T1_30100"), s("spouse.yob") <= 1958),
	 (Z(0), True)
	)

	e[35300] = s("spouse.T1_30500")

	e[35500] = sympy.Min(
	 Z(2000),
	 s("spouse.T1_31400"),
	)

	e[35700] = s("spouse.T1_31600")

	e[36000]# =

	e[6] = e[1] + e[2] + e[3] + e[4] + e[5]

	e[7] = sympy.Piecewise(
	 (s("spouse.T1_26000"), s("spouse.T1_26000") <= Z("53359")),
	 (s("spouse.T1_74") / Z("0.15") , True),
	)

	e[8] = s("spouse.T1_30000")

	e[9] = s("spouse.T1_100")

	e[10] = s("spouse.T1_32300")

	e[11] = e[8] + e[9] + e[10]

	e[36100] = sympy.Max(e[7] - e[11], Z(0))

	e[13] = sympy.Max(e[6] - e[12], Z(0))

	forms["T1"][32600] = e[13]

def apply_defaults(e: "Formyula"):
	e[36000] = 0
	pass

def apply_slips_information(e: "Formyula", slips):
	pass
