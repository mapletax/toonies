#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Computations related to taxes

import re

import sympy

from ...loonies.symbolic import Z


def incometax_qc(year, income):
	"""
	Compute income tax according to the tax return formula
	"""

	if year == 2023:
		# ref: TP-1.D.GR(2023-12)
		A = [0, 49275, 98540, 119910]
		R = [Z(x)/100 for x in [14, 19, 24, "25.75"]]
		K = [Z(x) for x in [0, "6898.50", "16258.85", "21387.65"]]
	elif year == 2022:
		# ref: TP-1.D.GR(2022-12)
		A = [0, 46295, 92580, 112655]
		R = [Z(x)/100 for x in [15, 20, 24, "25.75"]]
		K = [Z(x) for x in [0, "6944.25", "16201.25", "21019.25"]]
	else:
		raise NotImplementedError(year)

	args = []
	for a, r, k in [x for x in zip(A,R,K)][::-1]:
		args += [(((income - Z(a)) * Z(r) + Z(k)), income >= Z(a))]

	return sympy.Piecewise(*args)


def _build_annexes(descs, urls):
	out = dict()
	for desc, url in zip(descs, urls):
		m = re.match(r"^Annexe (?P<n>\S) – (?P<desc>.*)$", desc)
		nickname = "Anx" + m.group("n")
		name = "TP1-D." + m.group("n")
		basename = url.split("/")[-1].rsplit(".",1)[0].replace("%28", "(").replace("%29", ")")
		desc = m.group("desc")
		out[nickname] = (name, basename, desc)
	return out
