#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# TP-1.D 2022

import re

import sympy

from ...loonies.symbolic import Z

from .common import (
 incometax_qc,
 _build_annexes,
)

# https://www.revenuquebec.ca/fr/services-en-ligne/formulaires-et-publications/details-courant/tp-1/
annexes_desc = """
Annexe A – Montant pour personnes à charge et montant transféré par un enfant aux études
Annexe B – Allègements fiscaux
Annexe C – Crédit d'impôt pour frais de garde d'enfants
Annexe D – Crédit d'impôt pour solidarité
Annexe E – Redressements et crédits d'impôt
Annexe F – Cotisation au Fonds des services de santé
Annexe G – Gains et pertes en capital
Annexe H – Crédit d'impôt pour personne aidante
Annexe J – Crédit d'impôt pour maintien à domicile des aînés
Annexe K – Cotisation au régime d'assurance médicaments du Québec
Annexe L – Revenus d'entreprise
Annexe M – Intérêts payés sur un prêt étudiant
Annexe N – Rajustement des frais de placement
Annexe P – Crédits d'impôt relatifs à la prime au travail
Annexe Q – Revenus de retraite transférés à votre conjoint au 31 décembre
Annexe R – Cotisation au Régime québécois d'assurance parentale (RQAP)
Annexe S – Montant transféré par un enfant majeur aux études postsecondaires
Annexe T – Crédit d'impôt pour frais de scolarité ou d'examen et crédit d'impôt pour frais de scolarité ou d'examen transféré par un enfant
Annexe V – Crédits d'impôt pour dons
""".strip().splitlines()

annexes_urls = """
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.A%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.B%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.C%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.D%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.E%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.F%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.G%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.H%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.J%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.K%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.L%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.M%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.N%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.P%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.Q%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.R%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.S%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.T%282023-12%29.pdf
https://www.revenuquebec.ca/documents/fr/formulaires/tp/2023-12/TP-1.D.V%282023-12%29.pdf
""".strip().splitlines()

annexes = _build_annexes(annexes_desc, annexes_urls)

def s(x):
	return sympy.Symbol(x)

def formulate(e: "Formyula", year=2023):
	"""
	Reference: TP-1.D(2023-12).pdf
	"""

	# page 2

	e[154] = sympy.Add(
	 e[154.01],
	 e[154.02],
	 e[154.03],
	 e[154.04],
	 e[154.05],
	 e[154.06],
	 e[154.07],
	 e[154.08],
	 e[154.09],
	 e[154.10],
	 e[154.11],
	 e[154.12],
	 e[154.13],
	 e[154.14],
	 e[154.15],
	 e[154.16],
	 e[154.19],
	)

	e[199] = ( # revenu total
	   e[101]
	 + e[105]
	 + e[107]
	 + e[110]
	 + e[111]
	 + e[114]
	 + e[119]
	 + e[122]
	 + e[123]
	 + e[128]
	 + e[130]
	 + e[136]
	 + e[139]
	 + e[142]
	 + e[147]
	 + e[148]
	 + e[154]
	 + e[164]
	)

	months = s("qpp_months")

	# Ref: TP-1.D.GR(2023-12) § 248
	e_248_1 = sympy.Min(Z("4038.40"), e[98])
	e_248_2 = e_248_1 * Z("0.156250")
	e_248_3 = sympy.Min(Z(66600), e[98.1])
	e_248_4 = (Z(3500) * months / 12)
	e_248_5 = e_248_3 - e_248_4
	e_248_6 = e_248_5 * Z("0.01")
	e_248_7 = sympy.Min(e_248_2, e_248_6)
	e[248] = e_248_7

	e[250] = sympy.Add(
	 e[250.03],
	 e[250.04],
	 e[250.05],
	 e[250.06],
	 e[250.07],
	 e[250.08],
	 e[250.09],
	 e[250.11],
	 e[250.12],
	 e[250.13],
	 e[250.14],
	 e[250.15],
	 e[250.16],
	 e[250.17],
	)

	e[254] = ( # total deductions
	   e[201]
	 + e[205]
	 + e[207]
	 + e[214]
	 + e[225]
	 + e[228]
	 + e[231]
	 + e[234]
	 + e[236]
	 + e[241]
	 + e[245]
	 + e[246]
	 + e[248]
	 + e[250]
	 + e[252]
	)

	e[256] = e[199] - e[254]
	e[275] = sympy.Max(0, ( # revenu net
	   e[256]
	 + e[260]
	))


	# page 3

	# Revenu imposable
	e[279] = (
	   e[275]
	 + e[276]
	 + e[278]
	)
	e[298] = (
	   e[287]
	 + e[289]
	 + e[290]
	 + e[292]
	 + e[293]
	 + e[295]
	 + e[297]
	)
	# Revenu imposable
	e[299] = sympy.Max(0,
	   e[279]
	 - e[298]
	)

	# Crédits d'impôts non remboursables
	e[350] = Z(17183)
	e[359] = e[350] - e[358]
	e[377] = e[359] + e[361] + e[367] + e[376]
	e[377.1] = e[377] * 14 / 100


	e[388] = e[378] + e[381] + e[385]

	e[389] = e[388] * 20 / 100

	e[397] = e[397.1] * 10 / 100

	# Crédits d'impôts non remboursables
	e[399] = (
	   e[377.1]
	 + e[389]
	 + e[390]
	 + e[391]
	 + e[392]
	 + e[395]
	 + e[396]
	 + e[397]
	 + e[398]
	 + e[398.1]
	)

	# Impôt sur le revenu imposable
	e[401] = incometax_qc(year, e[299])

	e[406] = e[399]
	e[413] = e[401] - e[406]

	e[425] = e[414] + e[415] + e[422] + e[424]
	#

	e[430] = e[413] - e[425]

	e[432] = sympy.Max(0,
	   e[430]
	 - e[431]
	)


	# page 4

	# Impôts et cotisations
	e[450] = (
	   e[432]
	 + e[438]
	 + e[439]
	 + e[441]
	 + e[443]
	 + e[445]
	 + e[446]
	 + e[447]
	)

	e[451.2] = e[451] - e[451.1]

	# TODO
	e[452] = (
	 sympy.Piecewise( # RRQ
	  (e[98] - Z("4038.40"), e[98] > Z("4038.40")),
	  (0, True),
	 )
	 +
	 sympy.Piecewise( # RPC
	  (e[96] - Z("3754.45"), e[96] > Z("3754.45")),
	  (0, True),
	 )
	)

	e[465] = (
	   e[451.2]
	 + e[451.3]
	 + e[452]
	 + e[453]
	 + e[454]
	 + e[455]
	 + e[456]
	 + e[457]
	 + e[458]
	 + e[459]
	 + e[460]
	 + e[462]
	 + e[463]
	)

	e[468] = (
	   e[465]
	 + e[466]
	)

	e[470] = e[450] - e[468]

	e[474] = -sympy.Min(e[470], 0)

	e[478] = e[474] - e[476]

	e[475] = sympy.Max(e[470], 0)
	e[479] = e[475] - e[477]

	e[481] = e[479]

	return e


def apply_slips_information(forms, e, slips):
	"""
	Take values from slips and put them in the form

	Reference: TP-1.D(2023-12).pdf, look for the blue stuff.

	"""

	def s(x):
		return sympy.Symbol(x)

	e[96] = slips["RL-1"]["B-1"]
	e[96.1] = slips["RL-1"]["G-2"]
	e[97] = slips["RL-1"]["H"]
	e[98] = slips["RL-1"]["B"]
	e[98.1] = slips["RL-1"]["G"]
	e[100] = slips["RL-1"]["M"]
	e[101] = slips["RL-1"]["A"]
	e[102] = (
	   slips["RL-1"]["G-1"]
	 + slips["RL-1"]["L-2"]
	)
	e[105] = (
	   slips["RL-22"]["A"]
	 - slips["RL-1"]["P"]
	)


	vs = dict()
	codes_107 = dict()
	for slip in slips.slips:

		if (
		 slip.sliptype == "RL-1"
		 and (v := slip.get("O", 0)) > 0
		 and slip["meta"]["code"] == "RN"
		):
			e[107.02] = v
			codes_107["02"] = (
			 "02 Prestations d’assurance salaire (case O du relevé 1)"
			)

	e[107] = sympy.Add(
	 e[107.01],
	 e[107.02],
	 e[107.03],
	 e[107.04],
	 e[107.05],
	)

	if len(codes_107) == 0:
		e[106] = s("empty")
	elif len(codes_107) == 1:
		a = tuple(codes_107.keys())[0]
		e[106] = s(f"code_107_{a}")
	else:
		e[106] = s("code_107_09")

	e[110] = slips["RL-6"]["A"]
	# e[111] = slips["T4E"]
	e[119] = slips["RL-2"]["C"]
	e[147] = (
	   slips["RL-5"]["A"]
	 + slips["RL-5"]["B"]
	)


	#TP1.G § 154 Autres revenus

	vs = dict()
	codes_154 = dict()
	for slip in slips.slips:

		if (
		 slip.sliptype == "RL-1"
		 and (v := slip.get("O", 0)) > 0
		 and slip["meta"]["code"] == "RB"
		):
			vs[1] = v
			codes_154["01"] = "01 Bourses d’études (case O du relevé 1)"

		if (
		 slip.sliptype == "RL-1"
		 and (v := slip.get("O", 0)) > 0
		 and slip["meta"]["code"] == "RX" # TODO confirm
		):
			vs[2] = v
			codes_154["02"] = (
			 "02 Supplément de revenu reçu dans le cadre d’un programme"
			 " gouvernemental d’incitation au travail (case O du relevé 1)"
			)

		if (
		 slip.sliptype == "RL-1"
		 and (v := slip.get("O")) is not None
		 and slip["meta"]["code"] == "OR" # TODO
		):
			vs[3] = v
			codes_154["03"] = "03 Autres revenus (case O du relevé 1)"

		if (
		 slip.sliptype == "RL-2"
		 and (v := slip.get("C")) > 0
		):
			vs[6] = v
			codes_154["06"] = "06 Autres revenus (case C du relevé 2)"

		def get_sum(slip, cases):
			s = 0
			for x in cases:
				s += slip.get(x, 0)
			return s

		if (
		 slip.sliptype == "RL-2"
		 and (v := get_sum(slip, "DEGHK")) > 0
		):
			vs[7] = v
			codes_154["07"] = "07 Autres revenus (cases D, E, G, H et K du relevé 2)"

		if (
		 slip.sliptype == "RL-16"
		 and (v := get_sum(slip, "BG")) > 0
		):
			vs[8] = v
			codes_154["08"] = "08 Autres revenus (cases B et G du relevé 16)"

		if (
		 slip.sliptype == "RL-2"
		 and (v := get_sum(slip, "LO")) > 0
		):
			vs[9] = v
			codes_154["09"] = (
			 "09 Sommes retirées d’un REER dans le cadre"
			 " du Régime d’accession à la propriété (RAP)"
			 " ou du Régime d’encouragement à l’éducation"
			 " permanente (REEP) (cases L et O du relevé 2)"
			)

		if (
		 slip.sliptype == "RL-32"
		 and (v := get_sum(slip, "ABC")) > 0
		):
			vs[19] = v
			codes_154["19"] = (
			 "19 Compte d’épargne libre d’impôt pour l’achat"
			 " d’une première propriété (CELIAPP)"
			 " (cases A, B et C du relevé 32)"
			)

	if len(codes_154) == 0:
		e[153] = s("empty")
	elif len(codes_154) == 1:
		a = tuple(codes_154.keys())[0]
		e[153] = s(f"code_154_{a}")
	else:
		e[153] = s("code_154_66")

	e_201_1 = e[101] + e[105] + e[107] - slips["RL-1"]["211"]
	e_201_2 = 0 # TODO
	e_201_3 = 0
	e_201_4 = 0 # TODO
	e_201_5 = sympy.Max(s("AnxL_27"), 0)
	e_201_6 = (
	   e_201_1
	 + e_201_2
	 + e_201_3
	 + e_201_4
	 + e_201_5
	)
	e_201_7 = 0 # TODO
	e_201_8 = e_201_6 - e_201_7

	e[201] = sympy.Min(e_201_8 * Z("0.06"), Z(1315))

	e[205] = slips["RL-1"]["D"]
	e[422] = slips["RL-26"]["B"]
	e[441] = (
	   slips["RL-19"]["A"]
	 + slips["RL-19"]["B"]
	 + slips["RL-19"]["C"]
	 + slips["RL-19"]["D"]
	 + slips["RL-19"]["G"]
	 + slips["RL-19"]["H"]
	)

	e[451] = ( # Impôt du Québec retenu à la source, selon vos relevés ou vos feuillets
	   0
	 + slips["RL-1"]["E"]
	 # TODO
	)


def set_defaults(e: "Formyula"):
	e[96] = 0
	e[96.1] = 0
	e[105] = 0 # Correction des revenus d’emploi, si vous avez reçu un relevé 22 (grille de calcul 105)
	# Autres revenus d’emploi. Consultez le guide. 
	e[107.01] = 0
	e[107.02] = 0
	e[107.03] = 0
	e[107.04] = 0
	e[107.05] = 0

	e[111] = 0 # Prestations d’assurance emploi, feuillet T4E
	e[114] = 0 # Pension de sécurité de la vieillesse. Consultez le guide.
	e[122] = 0 # Prestations d’un régime de retraite, d’un REER, d’un FERR, d’un RPDB ou d’un RPAC/RVER, ou rentes
	e[123] = 0 # Revenus de retraite transférés par votre conjoint. Consultez le guide.
	e[128] = 0 # Dividendes de sociétés canadiennes imposables -- Montant imposable
	e[130] = 0 # Intérêts et autres revenus de placement
	e[136] = 0 # Revenus de location. -- Revenus nets
	e[139] = 0 # Gains en capital imposables (consultez le guide). Remplissez l’annexe G.
	e[142] = 0 # Pension alimentaire reçue (montant imposable)
	e[148] = 0 # Indemnités de remplacement du revenu et versement net des suppléments fédéraux.
	# Autres revenus. Consultez le guide.
	e[154.01] = 0
	e[154.02] = 0
	e[154.03] = 0
	e[154.04] = 0
	e[154.05] = 0
	e[154.06] = 0
	e[154.07] = 0
	e[154.08] = 0
	e[154.09] = 0
	e[154.10] = 0
	e[154.11] = 0
	e[154.12] = 0
	e[154.13] = 0
	e[154.14] = 0
	e[154.15] = 0
	e[154.16] = 0
	e[154.19] = 0
	e[164] = 0 # Revenus nets d’entreprise (montant de la ligne 34 de l’annexe L)
	e[165] = 0 # 
	e[166] = 0 # 
	e[167] = 0 # 
	e[168] = 0 # 
	e[169] = 0 # 
	e[207] = 0 # Dépenses d’emploi et déductions liées à l’emploi.
	e[212] = 0 # 
	e[214] = 0 # Déduction pour REER ou RPAC/RVER
	e[215] = 0 # 
	e[225] = 0 # Pension alimentaire payée (montant déductible). Consultez le guide.
	e[228] = 0 # Frais de déménagement. Remplissez le formulaire TP-348.
	e[231] = 0 # Frais financiers et frais d’intérêts. Consultez le guide aux lignes 231 et 260.
	e[233] = 0 # 
	e[234] = 0 # Perte à l’égard d’un placement dans une entreprise. Remplissez le formulaire TP-232.1.
	e[236] = 0 # Déduction pour particulier habitant une région éloignée reconnue (formulaire TP-350.1)
	e[241] = 0 # Déduction pour frais d’exploration et de mise en valeur
	e[245] = 0 # Déduction pour revenus de retraite transférés à votre conjoint au 31 décembre. Remplissez l’annexe Q. +
	e[246] = 0 # Déduction pour remboursement de sommes reçues en trop. Consultez le guide.

	# Autres déductions. Consultez le guide.
	e[249] = s("empty")
	e[250.03] = 0
	e[250.04] = 0
	e[250.05] = 0
	e[250.06] = 0
	e[250.07] = 0
	e[250.08] = 0
	e[250.09] = 0
	e[250.11] = 0
	e[250.12] = 0
	e[250.13] = 0
	e[250.14] = 0
	e[250.15] = 0
	e[250.16] = 0
	e[250.17] = 0

	e[252] = 0 # Report du rajustement des frais de placement. Consultez le guide.
	e[260] = 0 # Rajustement des frais de placement (consultez le guide). Remplissez l’annexe N.
	e[276] = 0 # Rajustement de déductions. Consultez le guide.
	e[278] = 0 # Prestation universelle pour garde d’enfants et revenus d’un régime enregistré d’épargne-invalidité. Consultez le guide.
	e[287] = 0 # Déductions pour investissements stratégiques. Consultez le guide.
	e[289] = 0 # Pertes d’autres années, autres que des pertes nettes en capital.
	e[290] = 0 # Pertes nettes en capital d’autres années. Consultez le guide aux lignes 276 (point 9) et 290. + 290
	e[292] = 0 # Déduction pour gains en capital. Consultez le guide.
	e[293] = 0 # Déduction pour Indien
	e[295] = 0 # Déductions pour certains revenus. Consultez le guide.
	e[297] = 0 # Déductions diverses. Consultez le guide.
	e[358] = 0
	e[361] = 0
	e[367] = 0
	e[376] = 0
	e[378] = 0 # frais pour soins médicaux non dispensés dans région
	e[381] = 0 # frais médicaux
	e[385] = 0 # intérêt payé sur prêt étudiant
	e[390] = 0 # crédit d'impôt pour pompier volontaire
	e[391] = 0 # crédit d'impôt pour prolongation de carrière
	e[392] = 0 # Crédit d’impôt pour nouveau diplômé travaillant dans une région ressource éloignée. Remplissez le formulaire TP-776.1.ND.
	e[395] = 0 # Crédits d’impôt pour dons. Consultez le guide.
	e[393] = 0 # Montant de la ligne 1 de la grille de calcul 395 393
	e[396] = 0 # Crédit d’impôt pour achat d’une habitation. Remplissez le formulaire TP-752.HA.
	e[397.1] = 0 # Crédit d’impôt pour cotisations syndicales, professionnelles ou autres
	e[398] = 0 # Crédit d’impôt pour frais de scolarité ou d’examen. Remplissez l’annexe T.
	e[398.1] = 0 # Crédit d’impôt pour frais de scolarité ou d’examen transféré par un enfant. Consultez le guide.
	e[414] = 0 # Crédit d’impôt pour contribution à des partis politiques autorisés du Québec (grille de calcul 414)
	e[415] = 0 # Crédit d’impôt pour dividendes
	e[422] = 0 # Crédits d’impôt pour actions de Capital régional et coopératif Desjardins, relevé 26, cases B et D + 422
	e[424] = 0 # Crédit d’impôt relatif à un fonds de travailleurs. Consultez le guide.
	e[431] = 0 # Crédits transférés d’un conjoint à l’autre. Consultez le guide.
	e[438] = 0 # Droits d’immatriculation au registre des entreprises. Consultez le guide.
	e[439] = 0 # Cotisation au Régime québécois d’assurance parentale (RQAP) pour un travail autonome ou hors du Québec. Remplissez l’annexe R. + 439
	e[443] = 0 # Impôts spéciaux et redressement d’impôt. Consultez le guide.
	e[445] = 0 # Cotisation au RRQ pour un travail autonome (grille de calcul 445)

	e[451.1] = 0 # Montant de la ligne 58 de votre annexe Q
	e[451.3] = 0 # Retraite - Impôt du Québec retenu à la source transféré à votre conjoint
	e[453] = 0 # Impôt payé par acomptes provisionnels
	e[454] = 0 # Partie transférable de l’impôt retenu pour une autre province
	e[455] = 0 # Crédit d’impôt pour frais de garde d’enfants. Remplissez l’annexe C.
	e[456] = 0 # Crédits d’impôt relatifs à la prime au travail. Remplissez l’annexe P.
	e[457] = 0 # Cotisation payée en trop au Régime québécois d’assurance parentale (RQAP)
	e[458] = 0 # Crédit d’impôt pour maintien à domicile des aînés. Remplissez l’annexe J.
	e[459] = 0 # Remboursement de TVQ à un salarié ou à un membre d’une société de personnes
	e[460] = 0 # Crédit d’impôt Bouclier fiscal
	e[462] = 0 # Autres crédits. Consultez le guide.
	e[463] = 0 # Crédit d’impôt pour soutien aux aînés
	e[466] = 0 # Compensation financière pour maintien à domicile. Consultez le guide.
	e[476] = 0 # Remboursement transféré au conjoint. Consultez le guide avant d’inscrire un montant.
	e[477] = 0 # Montant transféré par votre conjoint. Consultez le guide avant d’inscrire un montant.
	e[480] = 0 # Remboursement anticipé
