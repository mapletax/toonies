#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Annexe F (Health Services Fund) 2023 (Québec)

import sympy

from ...loonies.symbolic import Z


def formulate(forms, e: "Formyula", year=2023):
	"""
	Setup Annexe F (Health Services Fund) formulas.

	Reference: TP-1.D.F(2023-12).pdf
	"""

	threshold = Z("16780")

	def s(x):
		return sympy.Symbol(x)

	e[10] = s("TP1_199") + s("TP1_276")

	def p(x):
		return sympy.Piecewise(
		 (s("empty"), e[10] <= threshold),
		 (x, True),
		)

	e[12] = p(s("TP1_101"))

	e[14] = p(s("TP1_105"))

	e[16] = p(e[12] + e[14])

	e[18] = p(e[16] - e[10])

	def q(x):
		return sympy.Piecewise(
		 (s("empty"), e[10] <= threshold),
		 (s("empty"), e[18] <= threshold),
		 (x, True),
		)

	e[20] = q(s("TP1_107")) # TODO .3

	e[22] = q(s("TP1_114"))

	e[23] = q(s("TP1_128"))

	e[24] = q(s("TP1_166") + s("TP1_167"))

	e[25] = q(e[23] - e[24])

	e[26] = q(s("TP1_142"))

	e[28] = q(s("TP1_147"))

	e[29] = q(s("TP1_148"))

	e[30] = q(s("TP1_154"))

	e[31] = q(s("TP1_122"))

	e[33] = q(s("TP1_154"))

	e[34] = q(
	   e[20]
	 + e[22]
	 + e[25]
	 + e[26]
	 + e[28]
	 + e[29]
	 + e[30]
	 + e[31]
	 + e[33]
	)

	e[36] = q(e[18] - e[34])

	def r(x):
		return sympy.Piecewise(
		 (s("empty"), e[10] <= threshold),
		 (s("empty"), e[18] <= threshold),
		 (s("empty"), e[36] <= threshold),
		 (x, True),
		)

	e[41] = r(s("TP1_246"))

	e[42] = r(s("TP1_207"))

	e[43] = r(s("AnxR_26"))

	e[43.1] = r(s("TP1_445"))

	e[44] = r(s("TP1_250"))

	e[45] = r(s("TP1_250"))

	e[46] = r(s("TP1_245"))

	e[54] = r(s("TP1_225"))

	e[56] = r(s("TP1_231"))

	e[58] = r(s("TP1_234"))

	e[60] = r(s("TP1_293")) # TODO - e[16] - e[20] - e[25] - e[28]

	e[62] = r(s("TP1_297")) # TODO - e[12] - e[26]

	e[68] = r((
	   e[41]
	 + e[42]
	 + e[43]
	 + e[43.1]
	 + e[44]
	 + e[45]
	 + e[46]
	 + e[54]
	 + e[56]
	 + e[58]
	 + e[60]
	 + e[62]
	))

	e[70] = r(e[36] - e[68])

	def t(x):
		return sympy.Piecewise(
		 (s("empty"), e[10] <= threshold),
		 (s("empty"), e[18] <= threshold),
		 (s("empty"), e[36] <= threshold),
		 (s("empty"), e[70] <= threshold),
		 (x, True),
		)

	e[76] = t(e[70])

	threshold2 = 58350

	e[78] = t(sympy.Piecewise(
	 (sympy.Max(0, e[76] - Z(threshold)), e[76] <= Z(threshold2)),
	 (sympy.Max(0, e[76] - Z(threshold2)), True),
	))

	e[80] = t(e[78] * Z("0.01"))

	e[82] = t(sympy.Piecewise(
	 (sympy.Min(Z(150), e[80]), e[76] <= Z(58350)),
	 (sympy.Min(Z(1000), e[80] + Z(150)), True),
	))

	forms["TP-1.D"][446] = sympy.Piecewise(
	 (0, e[10] <= threshold),
	 (0, e[18] <= threshold),
	 (0, e[36] <= threshold),
	 (0, e[70] <= threshold),
	 (e[82], True),
	)
