#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Québec forms

import re
import os
import logging
import decimal
import collections
import datetime

import sympy

from ...loonies.symbolic import Z
from .. import formyula


logger = logging.getLogger(__name__)

qc_global = object()

class Forms:
	"""
	Holder for Québec forms
	"""
	def __init__(self, year=2022):
		self.year = year
		self.forms = {
		 qc_global: formyula.Formyula(""),
		}
		self._apply_slips_information = dict()
		self._slips = None
		self._receipts = None
		self._annexes = dict()
		self._files = dict()
		self.form_data = dict()

	def __setitem__(self, k, v):
		self.forms[k] = v

	def __getitem__(self, k):
		return self.forms[k]

	def apply_slips_information(self, allslips):
		self._slips = allslips
		for k, v in self.forms.items():
			if (func := self._apply_slips_information.get(k)) is None:
				continue
			func(self, v, allslips)

	def apply_identification(self, identification):
		self.identification = identification

		g = self.forms[qc_global]
		g["self_employed"] = 1 if identification["self-employed"] else 0
		g["spouse"] = 1 if identification["spouse"] else 0
		g = self.forms[qc_global]
		g["yob"] = identification["dob"].year
		province = identification["mailing"]["province"]
		province = identification.get("residence", dict()).get("province", province)
		g["residence_province_is_qc"] = 1 if province.upper() == "QC" else 0

		dependents = identification.get("dependents", dict())

		young_threshold = datetime.date(year=self.year-8, month=1, day=1)
		minor_threshold = datetime.date(year=self.year-18, month=1, day=1)

		impaireds = 0
		youngs = 0
		minor_or_infirms = 0
		for idx_dependent, (nickname, dependent_info) in enumerate(dependents.items()):
			dob = dependent_info["dob"]#, "%Y-%m-%d").date()
			if not dependent_info.get("child", True):
				continue

			is_impaired = dependent_info.get("impaired", False)
			if is_impaired:
				impaireds += 1
				continue

			is_young = dob >= young_threshold
			if is_young:
				youngs += 1
				continue

			is_minor = dob >= minor_threshold
			is_infirm = dependent_info.get("infirm", False)
			if is_infirm or is_minor:
				minor_or_infirms += 1
				continue

		g["nb_impaired"] = impaireds
		g["nb_young"] = youngs
		g["nb_minor_or_infirm"] = minor_or_infirms
		g["kids"] = youngs + minor_or_infirms + impaireds

		for idx_kid in range(10):
			g[f"kid{idx_kid+1}.revenue"] = Z(0) # TODO

		children = 1
		for idx_dependent, (nickname, dependent_info) in enumerate(dependents.items()):
			if not dependent_info.get("child", True):
				continue
			children += 1

	def apply_receipts_information(self, receipts):
		medical = 0
		for provider, info in receipts.items():
			for tp, entries in info.items():
				if tp == "healthcare-receipt":
					medical += entries["amount"]
				else:
					logger.warning("Skipping receipt %s %s", provider, tp)

		if medical > 0:
			logger.info("Adding %f of medical", medical)

		g = self.forms[qc_global]
		g["qc_medical_fees"] = Z(medical)

	def apply_overrides(self, overrides):
		for formname, formoverrides in overrides.items():
			if formname == "-":
				formname = qc_global

			try:
				form = self.forms[formname]
			except KeyError:
				logger.warning("Not applying overrides for form %s", formname)
				continue

			for k, v in formoverrides.items():
				logger.info("Applying %s %s=%s", formname, k, v)
				if isinstance(k, int):
					form[k] = Z(decimal.Decimal(v))
				else:
					if (m := re.match(r"page-(?P<page>\d+)", k)) is not None:
						page = int(m.group("page"))
						form_data = self.form_data.setdefault(formname, dict())
						page_data = form_data.setdefault(page, dict())
						for fieldid, value in v.items():
							if isinstance(value, bool):
								value = "Y"
							page_data[fieldid] = value
					else:
						try:
							form[k] = Z(decimal.Decimal(v))
						except Exception as e:
							raise ValueError(f"Can't set {formname}/{k} = {v}") from e


def get_forms(year):
	"""
	"""

	forms = Forms(year)

	if year == 2022:
		from . import tp1_d_2022 as tp1
	elif year == 2023:
		from . import tp1_d_2023 as tp1
	else:
		raise NotImplementedError(year)

	ftp1 = formyula.Formyula("TP1_")
	tp1.formulate(ftp1)
	tp1.set_defaults(ftp1)
	forms["TP-1.D"] = ftp1
	forms._apply_slips_information["TP-1.D"] = tp1.apply_slips_information

	if year == 2022:
		forms._files["TP-1.D"] = "TP-1.D(2022-12)"
	elif year == 2023:
		forms._files["TP-1.D"] = "TP-1.D(2023-12)"

	forms._annexes = tp1.annexes
	for nickname, (name, base, desc) in tp1.annexes.items():
		forms[nickname] = f = formyula.Formyula(nickname + "_")
		forms._files[nickname] = base
		anx = None
		if 0:
			pass
		elif nickname == "AnxB":
			if year == 2022:
				from . import annexe_b_2022 as anx
			if year == 2023:
				from . import annexe_b_2023 as anx
		elif nickname == "AnxC":
			if year == 2022:
				from . import annexe_c_2022 as anx
			if year == 2023:
				from . import annexe_c_2023 as anx
		elif nickname == "AnxF":
			if year == 2022:
				from . import annexe_f_2022 as anx
			if year == 2023:
				from . import annexe_f_2023 as anx
		elif nickname == "AnxK":
			if year == 2022:
				from . import annexe_k_2022 as anx
			elif year == 2023:
				from . import annexe_k_2023 as anx
		elif nickname == "AnxL":
			if year == 2022:
				from . import annexe_l_2022 as anx
			elif year == 2023:
				from . import annexe_l_2023 as anx
		elif nickname == "AnxR":
			if year == 2022:
				from . import annexe_r_2022 as anx
			elif year == 2023:
				from . import annexe_r_2023 as anx
		else:
			logger.warning("Annexe %s is not implemented yet", nickname)

		if anx is not None:
			anx.formulate(forms, f)

		try:
			forms._apply_slips_information[nickname] = anx.apply_slips_information
		except AttributeError:
			pass

	return forms


default = object()


def print_forms(forms, basename, annexes=None, filling=None):
	"""
	"""

	from ...formfiller.serialization import load
	from ...formfiller.draw import draw
	from ...formfiller.formfiller import collect_data
	from ..resolver import Resolver
	from ...loonies.currency import (
	 R2D,
	)

	identification = forms.identification
	serviettes = os.path.join(os.path.dirname(os.path.dirname(os.path.dirname(__file__))), "serviettes")

	resolver = Resolver(forms.forms.values())

	logger.info("Files: %s", forms._files)

	# Admissible children (< 16yo TODO or infirm)
	children = []
	dependents = identification.get("dependents", dict())
	for nickname, dependent in dependents.items():
		if dependent.get("child"): # logic ie wrong
			children.append(dependent)

	childcare_receipts = []
	for slip in forms._slips.slips:
		if slip.sliptype != "RL-24":
			continue
		childcare_receipts.append(slip)

	for nickname, base in forms._files.items():

		if annexes is not None:
			if nickname.startswith("Anx") and not nickname[3:] in annexes:
				continue

		form_filling = dict()
		if filling is not None:
			form_filling = filling.get(nickname, dict())

		year = forms.year
		fn = os.path.join(serviettes, str(forms.year), base)
		meta_path = fn + ".json"
		fill_path = fn + ".pdf"

		if not os.path.exists(meta_path):
			logger.warning("Missing form-filling meta: %s", meta_path)
			continue

		if not os.path.exists(fill_path):
			logger.warning("Missing form-filling base: %s", fill_path)
			continue

		meta = load(meta_path)

		e = forms[nickname]

		form_data = forms.form_data.setdefault(nickname, dict())
		for page, pageinfo in meta.items():
			page_data = form_data.setdefault(page, dict())
			logger.debug("Page: %s | fields: %s", page, len(pageinfo["fields"]))
			field_copy_number = collections.defaultdict(int)

			for k, v in pageinfo["fields"].items():
				if "value" in v:
					del v["value"]

				value = default



				if (m := re.match(r"checkbox-(?P<n>[^/]+)(/\d+)?", k)) is not None:
					ref = float(m.group("n"))
					if ref == int(ref):
						ref = int(ref)
					sym = e[ref]

					field_copy_number[sym] += 1

					filling_value = form_filling.get(sym.name[len(getattr(e, "_prefix")):], default)
					if filling_value is not default:
						if isinstance(filling_value, list):
							value = filling_value[field_copy_number[sym]-1]
						else:
							value = filling_value

					x = resolver(sym)

					if isinstance(x, sympy.Symbol):
						logger.warning("%s (%s) is not calculated yet (%s)", k, sym, x)
					else:
						if isinstance(x, (
						 bool,
						 sympy.logic.boolalg.BooleanTrue,
						 sympy.logic.boolalg.BooleanFalse,
						 sympy.core.numbers.One,
						 sympy.core.numbers.Zero,
						 )):
							value = "Y" if x else None
						else:
							raise NotImplementedError(f"checkbox with {type(x)}/{x}")

				elif (m := re.match(r"numeric-(?P<n>[^/]+)(/\d+)?", k)) is not None:
					ref = float(m.group("n"))
					if ref == int(ref):
						ref = int(ref)
					sym = e[ref]

					field_copy_number[sym] += 1

					filling_value = form_filling.get(sym.name[len(getattr(e, "_prefix")):], default)
					if filling_value is not default:
						if isinstance(filling_value, list):
							value = filling_value[field_copy_number[sym]-1]
						else:
							value = filling_value

					x = resolver(sym)

					#if sym.name == "TP1_447":
					#	1/0
					if isinstance(x, int):
						value = x
					else:
						try:
							value = R2D(x)
						except (TypeError, NotImplementedError) as exc:
							#form_data[page][k] = float("NaN")
							logger.warning("%s (%s) is not calculated yet (%s): %s", k, sym, x, exc)

				elif (m := re.match(r"(?P<tp>char|text)-(?P<n>[^/]+)(?P<c>/\d+)?", k)) is not None:
					ref = float(m.group("n"))
					if ref == int(ref):
						ref = int(ref)

					field_copy_number[ref] += 1

					if nickname == "AnxC" and year == 2022:
						ns_1 = { (1+_): _ for _ in range(6-1+1) }
						ns_2_firstname = { round(29.1+_*0.1,1): _ for _ in range(8-1+1) }
						ns_2_dob = { (30+_): _ for _ in range(37-30+1) }
						ns_2_business = { round(30.2+_, 1): _ for _ in range(37-30+1) }
						if (idx_child := ns_1.get(ref)) is not None:
							if idx_child < len(children):
								kid = children[idx_child]
								if field_copy_number[ref] == 1:
									value = "{} {}".format(kid["first-name"], kid["last-name"])
								elif field_copy_number[ref] == 2:
									value = kid["relation"]
								elif field_copy_number[ref] == 3:
									value = None
							else:
								value = None
						elif (idx_receipt := ns_2_firstname.get(ref)) is not None:
							if idx_receipt < len(childcare_receipts):
								receipt = childcare_receipts[idx_receipt]
								value = receipt.values["child"]["first-name"]
							else:
								value = None
						elif (idx_receipt := ns_2_dob.get(ref)) is not None:
							if idx_receipt < len(childcare_receipts):
								receipt = childcare_receipts[idx_receipt]
								value = str(receipt.values["child"]["dob"]).replace("-", "")
							else:
								value = None
						elif (idx_receipt := ns_2_business.get(ref)) is not None:
							if idx_receipt < len(childcare_receipts):
								receipt = childcare_receipts[idx_receipt]
								value = str(receipt.values["H"])
							else:
								value = None

					if nickname == "AnxC" and year == 2023:
						ns_1 = { (1+_): _ for _ in range(6-1+1) }
						ns_2_firstname = { round(26.3+_,1): _ for _ in range(37-26+1) }
						ns_2_dob = { (26+_): _ for _ in range(37-26+1) }
						ns_2_business = { round(26.2+_, 1): _ for _ in range(37-26+1) }
						if (idx_child := ns_1.get(ref)) is not None:
							if idx_child < len(children):
								kid = children[idx_child]
								if m.group("tp") == "text" and m.group("c") is None:
									value = "{} {}".format(kid["first-name"], kid["last-name"])
								elif m.group("tp") == "text" and m.group("c") == "/1":
									value = kid["relation"]
								elif field_copy_number[ref] == 3:
									value = None
							else:
								value = None
						elif (idx_receipt := ns_2_firstname.get(ref)) is not None:
							if idx_receipt < len(childcare_receipts):
								receipt = childcare_receipts[idx_receipt]
								value = receipt.values["child"]["first-name"]
							else:
								value = None
						elif (idx_receipt := ns_2_dob.get(ref)) is not None:
							if idx_receipt < len(childcare_receipts):
								receipt = childcare_receipts[idx_receipt]
								value = str(receipt.values["child"]["dob"]).replace("-", "")
							else:
								value = None
						elif (idx_receipt := ns_2_business.get(ref)) is not None:
							if idx_receipt < len(childcare_receipts):
								receipt = childcare_receipts[idx_receipt]
								value = str(receipt.values["H"])
							else:
								value = None

					if value is default:

						sym = e[ref]

						field_copy_number[sym] += 1

						if value is default:
							filling_value = form_filling.get(sym.name[len(getattr(e, "_prefix")):], default)
							if filling_value is not default:
								if isinstance(filling_value, list):
									value = filling_value[field_copy_number[sym]-1]
								else:
									value = filling_value

						print_char = False
						if nickname == "AnxC" and ref == 92:
							print_char = True
						if nickname == "TP-1.D" and ref == 449:
							print_char = True

						if value is default and print_char:
							x = resolver(sym)

							if isinstance(x, int):
								value = x
							else:
								try:
									value = R2D(x)
								except (TypeError, NotImplementedError) as exc:
									logger.warning("%s (%s) is not calculated yet (%s): %s", k, sym, x, exc)

				aliases = v.get("aliases", dict())
				if identification is not None and (alias := aliases.get("identity")) is not None:

					logger.info("identity: %s", alias)

					def dig(root, path):
						cur, *res = path
						if not res:
							return root[cur]
						return dig(root[cur], res)

					spouse_p = identification["marital-status"] in ("married", "common-law")

					if 0:
						pass
					elif alias == "yes-spouse" and spouse_p:
						value = True
					elif alias == "no-spouse" and not spouse_p:
						value = True
					elif alias == "lang-fr" and identification["language"].lower() == "fr":
						value = True
					elif alias == "lang-en" and identification["language"].lower() == "en":
						value = True
					elif alias == "sex-male" and identification["gender"].upper() == "M":
						value = True
					elif alias == "sex-female" and identification["gender"].upper() == "F":
						value = True
					elif alias == "mailing/street-name-po-box":
							mailing = identification.get("mailing", dict())
							street = mailing.get("street")
							if street:
								value = f"{street}"
							# TODO PO box
					else:
						try:
							value = dig(identification, alias.split("/"))
						except KeyError:
							pass

					if alias in ("sin", "mailing/postal-code", "spouse/sin") and value is not default:
						value = str(value).replace(" ", "")
					elif alias in ("dob", "dod", "spouse/dob", "spouse/dod") and value is not default:
						value = str(value).replace("-", "")
					elif alias in ("phone-number") and value is not default:
						value = str(value).translate(str.maketrans({"(": "", ")": "", "-": "", " ": ""}))

					if (value_ := form_filling.get(alias, default)) is not default:
						if isinstance(value, list):
							value = value_[field_copy_number[sym]-1]
						else:
							value = value_

					if isinstance(value, bool):
						value = "Y" if value else ""

					if value is default:
						logger.warning("Couldn't resolve identity field %s", k)

				if value is default:
					pass
				elif value is None:
					pass
				else:
					page_data[k] = value

		data = collect_data(meta, form_data)
		data = list(data)

		if nickname == "TP-1.D":

			if (signature := identification.get("signature")) is not None:
				data[-1]["fields"]["signature"] = {
				 #"debug-show-bbox": (1, (0, 1, 0, 1)),
				 "bounding-box": [110,630,280,25],
				 "value": signature,
				}

			if (date := identification.get("date")) is not None:
				data[-1]["fields"]["date"] = {
				 #"debug-show-bbox": True,
				 "bounding-box": [410,630,160,25],
				 "align": "center",
				 "font-color": [0, 0, 1, 1],
				 "value": date,
				}

		#logging.getLogger("mapletax.formfiller.draw").setLevel(logging.WARNING)

		dest = f"{basename}-{nickname}.pdf"
		draw(data, dest, fill_path)
