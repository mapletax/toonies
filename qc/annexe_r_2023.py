#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Annexe R (RQAP) 2023

import logging
import re

import sympy

from ...loonies.symbolic import Z


logger = logging.getLogger(__name__)


def formulate(forms, e: "Formyula", year=2023):
	"""
	Setup Annexe R formulas.

	Reference: TP-1.D.R(2023-12).pdf
	"""

	logger.warning("Annexe R not implemented, assuming employee status")
	return

	def s(x):
		return sympy.Symbol(x)

	# page 1

	# section A

	e[10] = 0#s("AnxL_27")
	e[11] = 0#s("AnxL_40")
	e[12] = e[10] + e[11]
	e_13_ = Z(91000)

	e[16] = e[32]
	e[18] = e[14] + e[16]
	e[20] = sympy.Max(0, e_13_ - e[18])

	e[22] = sympy.Min(e[12], e[20])
	e[24] = sympy.Min(Z("798.98"), e[22] * Z("0.00878"))

	forms["TP-1.D"][439] = e[24]

	e[26] = e[24] * Z("0.43736")
	forms["TP-1.D"][248] = e[26]

	# section B

	# TODO
	e[30] = 0
	e[31] = 0
	e[32] = 0


def apply_slips_information(forms, e, allslips):
	e[14] = allslips["RL-1"]["I"] # TODO or A if not I!
