#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Annexe K (RAMQ) 2023 (Québec)

import re

import sympy

from ...loonies.symbolic import Z


def formulate(forms, e: "Formyula", year=2023):
	"""
	Setup Annexe K (RAMQ) formulas.

	Reference: TP-1.D.K(2023-12).pdf
	"""

	def s(x):
		return sympy.Symbol(x)

	# page 1

	e[36] = s("TP1_275")
	e[37] = s("spouse.TP1_275")

	# Revenu total
	e[40] = e[36] + e[37]

	e[41] = sympy.Piecewise(
	 (Z(30640), s("spouse") > 0),
	 (Z(18910), True),
	)

	e[42] = sympy.Piecewise(
	 (Z(7510), sympy.And(s("spouse") > 0, s("kids") > 1)),
	 (Z(3905), sympy.And(s("spouse") > 0, s("kids") > 0)),
	 (Z(0),    True),
	)

	e[44] = sympy.Piecewise(
	 (Z(11730), sympy.And(sympy.Not(s("spouse") > 0), s("kids") > 0)),
	 (Z(15635), sympy.And(sympy.Not(s("spouse") > 0), s("kids") > 1)),
	 (Z(0),     True),
	)

	e[46] = e[41] + e[42] + e[44]

	e[48] = sympy.Max(0, e[40] - e[46])

	e[60] = (
	   s("ramq_month_01")
	 + s("ramq_month_02")
	 + s("ramq_month_03")
	 + s("ramq_month_04")
	 + s("ramq_month_05")
	 + s("ramq_month_06")
	)

	e[61] = (
	   s("ramq_month_07")
	 + s("ramq_month_08")
	 + s("ramq_month_09")
	 + s("ramq_month_10")
	 + s("ramq_month_11")
	 + s("ramq_month_12")
	)

	e[62] = e[60] + e[61]

	# page 2

	e[74] = (
	   s("spouse.ramq_month_01")
	 + s("spouse.ramq_month_02")
	 + s("spouse.ramq_month_03")
	 + s("spouse.ramq_month_04")
	 + s("spouse.ramq_month_05")
	 + s("spouse.ramq_month_06")
	)

	e[75] = (
	   s("spouse.ramq_month_07")
	 + s("spouse.ramq_month_08")
	 + s("spouse.ramq_month_09")
	 + s("spouse.ramq_month_10")
	 + s("spouse.ramq_month_11")
	 + s("spouse.ramq_month_12")
	)

	e[76] = e[74] + e[75]

	e[77] = e[48]

	e[78] = sympy.Piecewise(
	 (Z(0), e[48] <= Z(5000)),
	 (Z(5000), e[48] <= Z(14671)),
	 (Z(0), True),
	)

	e[79] = e[77] - e[78]

	e[80] = sympy.Piecewise(
	 (Z("0.0747"), sympy.And(sympy.Not(s("spouse") > 0), e[48] <= Z(5000))),
	 (Z("0.1122"), sympy.And(sympy.Not(s("spouse") > 0), e[48] <= Z(14671))),
	 (Z("0.0375"), sympy.And(s("spouse") > 0, e[48] <= Z(5000))),
	 (Z("0.0562"), sympy.And(s("spouse") > 0, e[48] <= Z(14671))),
	 (0, True),
	)

	e[81] = e[79] * e[80]

	e[82] = sympy.Piecewise(
	 (Z(0), sympy.And(sympy.Not(s("spouse") > 0), e[48] <= Z(5000))),
	 (Z("373.5"), sympy.And(sympy.Not(s("spouse") > 0), e[48] <= Z(14671))),
	 (Z(0), sympy.And(s("spouse") > 0, e[48] <= Z(5000))),
	 (Z("187.5"), sympy.And(s("spouse") > 0, e[48] <= Z(14671))),
	 (Z(0), True),
	)

	e[83] = sympy.Min(731, e[81] + e[82])

	e[84] = sympy.Piecewise(
	 (e[83], e[48] <= Z(14671)),
	 (Z(731), True)
	)

	e[85] = e[84] * e[62] / 12

	e[86] = e[84] - e[85]

	e_87 = Z("720.5")

	e[88] = e[60] * Z("59.17") + e[61] * Z("60.92")

	e[89] = e_87 - e[88]

	e[90] = sympy.Min(e[86], e[89])

	_pay_spouse_ramq = sympy.And(
	 e["pay_spouse"] < 1,
	)

	def t(x):
		return sympy.Piecewise(
		 (s("empty"), _pay_spouse_ramq),
		 (x, True),
		)

	e[91] = t(e[84])

	e[92] = t(e[91] * e[76] / 12)

	e[93] = t(e[91] - e[92])

	e_94_ = Z("720.5")

	e[95] = t(e[74] * Z("59.17") + e[75] * Z("60.92"))

	e[96] = t(e_94_ - e[95])

	e[97] = t(sympy.Min(e[93], e[96]))
	e_97_ = sympy.Piecewise(
	 (0, _pay_spouse_ramq),
	 (e[97], True),
	)

	e[98] = sympy.Piecewise(
	 (e[90] + e_97_, e["code"] < 1),
	 (0, True),
	)

	forms["TP-1.D"][447] = e[98]
	forms["TP-1.D"][449] = sympy.Piecewise(
	 (e["code"], e["code"] > 0),
	 (s("empty"), True),
	)
