#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import re

import sympy

from ...loonies.symbolic import Z


def formulate(forms, e: "Formyula", year=2022):
	"""
	Setup Annexe L formulas.

	Reference: TP-1.D.L(2022-12).pdf
	"""


def apply_slips_information(forms, e, allslips):
	"""
	"""

	e[22] = 0
	e[23] = 0
	e[24] = 0
	e[25] = 0
	e[26] = 0
	e[27] = 0
