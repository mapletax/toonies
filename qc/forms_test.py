#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Test

import os
import logging
import collections

import pytest

from .forms import *

logger = logging.getLogger(__name__)


def test_qc():
	forms = get_forms(2022)
	for k, v in forms.forms.items():
		logger.info("- %s: %s", k, v)
