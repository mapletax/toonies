#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Annexe C (GARDE) 202C (Québec)

import re

import sympy

from ...loonies.symbolic import Z


def formulate(forms, e: "Formyula", year=2022):
	"""
	Setup Annexe C formulas.

	Reference: TP-1.D.C(2022-12).pdf
	"""

	def s(x):
		return sympy.Symbol(x)

	# page 1

	# section A1

	e[1] = s("kid1.revenue")
	e[2] = s("kid2.revenue")

	employment_income = s("T1_10100")
	employed = employment_income > 0
	spouse_employment_income = s("spouse.T1_10100")
	spouse_employed = spouse_employment_income > 0

	e[10] = sympy.Or(employed, spouse_employed)

	# section A2


	e[40] = 0 # releve 1 & 5

	e[41] = e[39] - e[40]

	# section B

	e[42] = s("nb_impaired")
	e[43] = Z(14605) * e[42]

	e[44] = s("nb_young")
	e[45] = Z(10675) * e[44]

	e[46] = s("nb_minor_or_infirm")
	e[47] = Z(5375) * e[46]

	e[50] = e[43] + e[45] + e[47]

	# section C

	e[76] = s("TP1_275")
	e[78] = s("spouse.TP1_275")

	e[80] = e[76] + e[78]

	# section D

	e[85] = sympy.Min(e[41], e[50])

	e[92] = sympy.Piecewise(
	 (Z("78"), sympy.And(e[80] > 0, e[80] <= Z(21555))),
	 (Z("75"), sympy.And(e[80] > Z(21555), e[80] <= Z(38010))),
	 (Z("74"), sympy.And(e[80] > Z(38010), e[80] <= Z(39415))),
	 (Z("73"), sympy.And(e[80] > Z(39415), e[80] <= Z(40830))),
	 (Z("72"), sympy.And(e[80] > Z(40830), e[80] <= Z(42220))),
	 (Z("71"), sympy.And(e[80] > Z(42220), e[80] <= Z(43635))),
	 (Z("70"), sympy.And(e[80] > Z(43635), e[80] <= Z(104170))),
	 (Z("67"), e[80] > Z(104170)),
	)

	e[94] = e[85] * e[92] * Z("0.01")

	e[96] = s("spouse.TP1_455")

	e[98] = e[94] - e[96]

	forms["TP-1.D"][455] = e[98]


def apply_slips_information(forms, e, allslips):
	"""
	"""

	s = 0
	idx_rl24 = 0
	for slip in allslips.slips:
		if slip.sliptype != "RL-24":
			continue

		e[round(idx_rl24 + 30.1, 1)] = slip.values["E"]
		s += slip.values["E"]

		idx_rl24 += 1

	e[39] = s
