#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Annexe B (frais médicaux) 2023

import re

import sympy

from ...loonies.symbolic import Z


def formulate(forms, e: "Formyula", year=2023):
	"""
	Setup Annexe B formulas.

	Reference: TP-1.D.B(2022-12).pdf
	"""

	def s(x):
		return sympy.Symbol(x)


	# Ref: TP-1.G § 462 Autres crédits
	# § Grille de calcul – Crédit d’impôt remboursable pour frais médicaux
	e[0.01] = s("TP1_101") + s("TP1_105") + s("TP1_107")
	e[0.03] = s("TP1_107.02")
	e[0.04] = s("TP1_205") + s("TP1_207")
	e[0.05] = e[0.02] + e[0.03] + e[0.04]
	e[0.06] = sympy.Max(0, e[0.02] - e[0.04])
	e[0.07] = s("AnxL_22") + s("AnxL_23") + s("AnxL_24") + s("AnxL_25") + s("AnxL_26")
	e[0.08] = s("TP1_154.02")
	e[0.09] = s("TP1_154.12")
	e[0.10] = e[0.06] + e[0.07] + e[0.08] + e[0.09]

	# page 1


	# A
	e[10] = s("TP1_275")
	e[12] = s("spouse.TP1_275")
	e[14] = e[10] + e[12]

	# B

	# page 2

	# C
	e[36] = s("qc_medical_fees") + s("AnxK_98")
	e[37] = e[14]
	e[39] = e[37] * Z("0.03")
	e[40] = sympy.Max(0, e[36] - e[39])
	forms["TP-1.D"][381] = e[40]

	# D
	e[41] = e[40]
	e[42] = s("TP1_250.07")
	e[43] = e[41] + e[42]
	e[44] = sympy.Min(e[43] * Z("0.25"), Z(1356))
	e[45] = e[14]
	e[47] = sympy.Max(e[45] - Z(26220), 0)
	e[48] = e[47] * Z("0.05")
	e[50] = sympy.Max(e[44] - e[48], 0)

	forms["TP-1.D"][462] = sympy.Piecewise(
	 (0, e[0.10] > 3470),
	 (0, e[14] > 53340),
	 (e[50], True)
	)

	# E

def apply_slips_information(forms, e, allslips):
	"""
	"""

	e[0.02] = allslips["RL-1"][211]
