#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2023-2024 Jérôme Carretero <cJ-mapletax@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Syntactic sugar for equations related to taxes

import logging
import typing as t
import numbers
import sympy


logger = logging.getLogger(__name__)


class Formyula:
	"""
	A class that holds symbols and their values

	This class allows to create namespaced symbols, while providing
	not-fully-qualified local references to them: creating `f = Formyula("x_")`
	then accessing `f[1]` manipulates the `x_1` symbol.

	Furthermore, setting `f[1]` will both declare `x_1` and associate
	a right hand side (RHS) value to it.

	Furthermore, aliases are possible: when an alias is declared
	(it must be declared prior to setting any value), a symbol may be referred
	to by one or more other references.
	This is added because Federal forms have both stable 5-digit identifiers,
	and line numbers which may vary from one year to the other.
	"""
	def __init__(self, prefix=None):
		if prefix is None:
			prefix = "line_"
		super().__setattr__("_prefix", prefix)
		super().__setattr__("__values", dict())
		super().__setattr__("__n2s", dict())
		super().__setattr__("__aliases", dict())

	def __str__(self):
		return "(Symbox with {})".format(",".join(self.n2s().keys()))

	def add_alias(self, target, equivalent):
		aliases = getattr(self, "__aliases")
		for s in equivalent:
			aliases[s] = target

	def values(self):
		return getattr(self, "__values")

	def items(self):
		return getattr(self, "__values").items()

	def value(self, s: sympy.Symbol):
		return getattr(self, "__values")[s]

	def n2s(self):
		return getattr(self, "__n2s")

	def __getattr__(self, name):
		return getattr(self, "__getitem__")(name)

	def __setattr__(self, name, value):
		return getattr(self, "__setitem__")(name, value)

	def __getitem__(self, name):
		aliases = getattr(self, "__aliases")
		name = aliases.get(name, name)

		prefix = getattr(self, "_prefix")
		name = f"{prefix}{name}"

		s = sympy.Symbol(name)
		getattr(self, "__n2s")[name] = s
		super().__setattr__(name, s)
		return s

	def get(self, name, default):
		aliases = getattr(self, "__aliases")
		name = aliases.get(name, name)

		prefix = getattr(self, "_prefix")
		name = f"{prefix}{name}"

		if name in getattr(self, "__n2s"):
			return self[name]
		else:
			return default

	def __setitem__(self, name, value):
		aliases = getattr(self, "__aliases")
		name = aliases.get(name, name)

		prefix = getattr(self, "_prefix")
		name = f"{prefix}{name}"

		n2s = getattr(self, "__n2s")
		if isinstance(value, sympy.Symbol) and value.name == name:
			s = value
			for k, v in self.values().items():
				if isinstance(v, sympy.Expr):
					modified = False
					for a in v.atoms():
						if isinstance(a, sympy.Symbol) and a.name == name:
							logger.debug("Replace %s (%s) with %s (%s)",
							 a, a.assumptions0,
							 s, s.assumptions0,
							)
							v = v.subs(a, s)
					if modified:
						values[k] = v
			super().__setattr__(name, s)

		else:
			values = getattr(self, "__values")
			s = n2s.get(name)
			if s is None:
				s = sympy.Symbol(name)
				n2s[name] = s
				super().__setattr__(name, s)

			if value != s:
				values[s] = value

