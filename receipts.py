#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Rostyslav Lobov <rostyslav-mapletax@exmakhina.com> & contributors
# SPDX-Licence-Identifier: AGPL-3.0-only

import typing as t
import logging
import numbers
import functools
import re

from ..loonies.symbolic import Z


logger = logging.getLogger(__name__)


def known_receipt_type_p(name: str):
	return name in {
	 "medical",
	 "adoption",
	 "digital-news-subscription",
	 "home-accessibility",
	 "school-supplies",
	}


class Receipt:
	"""
	Class to store the receipt data. It is possible to assign the data upon creation,
	by passing the dict of values or by passing values as a key-word arguments
	"""
	def __init__(self,
	 receipttype: str,
	 values: t.Mapping[str, numbers.Number]=None,
	 **kw
	):
		"""
		:param receipttype: Type of the receipt, eg. "medical"
		:param values: Dict with receipt data.
		"""

		if not known_receipt_type_p(receipttype):
			raise ValueError(f"Unknown receipt type {receipttype}")

		self.receipttype = receipttype

		self.values = dict()
		if values is not None:
			for k,v in values.items():
				self[k] = v
		for k, v in kw.items():
			self[k] = v

	def update(self, d):
		self.values |= d

	def __setitem__(self, k, v):
		if isinstance(v, float):
			v = 100 * v
			if v != int(v):
				raise ValueError(v)
			v = Z(int(v)) / 100
		else:
			v = Z(v)

		self.values[k] = v

	def __getitem__(self, k):
		return self.values[k]

	def get(self, k, default=None):
		return self.values.get(k, default)


class ReceiptSummationProxy:
	"""
	Proxy for Receipts '__getitem__' method.
	Intended to return the sum of the corresponding receipt values
	"""
	def __init__(self, receipts, selected):
		self.receipts = receipts
		self.selected = selected

	def __getitem__(self, k):
		res = 0
		for receipt in self.selected:
			res += receipt.get(k, 0)
		return res


class Receipts:
	"""Container, intended to be initialized with all of a user's receipts"""
	def __init__(self, receipts: t.Sequence[Receipt]):
		self.receipts = receipts

	def __getitem__(self, k):
		selected = []
		for receipt in self.receipts:
			if receipt.receipttype == k:
				selected += [receipt]
		return ReceiptSummationProxy(self, selected)

def receipts_from_info(data):
	"""
	Create a Receipts instance from data structured like:

	.. code:: yaml

	   Doctor Foo #123:
	     healthcare-receipt:
	       number: ...

	"""
	receipts = list()
	for source, info in data.items():
		for receipttype, receiptdata in info.items():
			receipt = Receipt(receipttype, receiptdata)
			receipts.append(receipt)
	return Receipts(receipts)
