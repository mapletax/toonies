#!/usr/bin/env python
# SPDX-FileCopyrightText: 2024 Rostyslav Lobov <rostyslav-mapletax@exmakhina.com> & contributors
# SPDX-Licence-Identifier: AGPL-3.0-only

import logging
import numbers
import typing as t

from .formyula import *


logger = logging.getLogger(__name__)


def test_formyula_basic():
	form = Formyula()
	form[1] = 2

	form[2] = form[1]

	form.add_alias(1, {45})

	logger.info("form[45] = %s", form[45])
