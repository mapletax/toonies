#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2023,2024 Jérôme Carretero <cJ-mapletax@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only
# Basic resolver

import logging
import typing as t
import functools

import sympy


logger = logging.getLogger(__name__)


class Resolver:
	def __init__(self, formyulas):
		self.formyulas = formyulas
		self.cache = dict()

	def __call__(self, s: sympy.Symbol,
	  stop_at: t.Optional[t.Set[sympy.Symbol]]=None,
	  maxdepth=100,
	 ):
		if (c := self.cache.get(s)) is not None:
			return c

		e = self.formyulas

		logger.debug("Resolve %s in %s?", s, e)

		if maxdepth == 0:
			logger.warning("Recursion limit reached -> returning symbol %s", s)
			for f in e:
				if s.name in f.n2s():
					ret = f.n2s()[s.name]
					self.cache[s] = ret
					return ret
		for f in e:
			if s in f.values():
				expr = f.values()[s]
				logger.debug("Found in %s (%s) %s", f, s, expr)
				break
		else:
			logger.debug("Not found %s in %s -> returning symbol", s, e)
			for f in e:
				if s.name in f.n2s():
					ret = f.n2s()[s.name]
					self.cache[s] = ret
					return ret
			return KeyError(f"Symbol of name {s.name} unknown")

		if stop_at is None:
			stop_at = set()

		try:
			expr = expr.doit()
		except AttributeError:
			ret = expr
			self.cache[s] = ret
			return ret

		logger.debug("%s has expression %s", s, expr)

		#logger.debug("variables %s", variables)
		subs = dict()
		for s_ in expr.atoms():
			if not isinstance(s_, sympy.Symbol):
				logger.debug("Atom %s not a symbol", s_)
				continue

			logger.debug("Where is %s?", s_)
			for f in e:
				logger.debug(" %s %s", f, f.n2s())
				if s_.name in f.n2s():
					break
			else:
				continue

			s_ = f.n2s()[s_.name]

			if s_ in stop_at:
				continue

			s__ = self.__call__(s_, stop_at, maxdepth=maxdepth-1)

			if s__ != s_:
				subs[s_] = s__

		if subs:
			logger.debug("Subs:")
			for k, v in subs.items():
				logger.debug("- %s = %s", k, v)


			logger.debug("<- %s", expr)

			ret = expr.subs(subs)

			logger.debug("-> %s", ret)

			assert ret is not expr, f"Didn't substitute {expr} with {subs}"
		else:
			logger.debug("No subs for %s", expr)
			ret = expr

		self.cache[s] = ret
		return ret
