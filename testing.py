#!/usr/bin/env python
# -*- coding: utf-8
# SPDX-FileCopyrightText: 2024 Jérôme Carretero <cJ-toonies@zougloub.eu> & contributors
# SPDX-License-Identifier: AGPL-3.0-only

import os

import pytest

def requires_serviettes():
    serviettes = os.path.join(os.path.dirname(os.path.dirname(__file__)), "serviettes")
    return pytest.mark.skipif(not os.path.isdir(serviettes),
     reason=f"Missing serviettes at {serviettes}",
    )
